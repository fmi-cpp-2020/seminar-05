#include "catch2.hpp"
#include "vector.hpp"


TEST_CASE("Vector initialization","[vectorInit]") {
	const vector vec(1);
	
	REQUIRE(vec.getCapacity() == 1);
	REQUIRE(vec.getSize() == 0);
}

TEST_CASE("Insert last element into vector", "[vectorInsertLastElement]") {
	vector vec;

	SECTION("Without resize") {
		vec.insertLastElement(0);

		REQUIRE(vec.getCapacity() == 1);
		REQUIRE(vec.getSize() == 1);
		REQUIRE(vec.getData()[0] == 0);
	}

	SECTION("With resize") {
		vec.insertLastElement(0);
		vec.insertLastElement(1);

		REQUIRE(vec.getCapacity() == 2);
		REQUIRE(vec.getSize() == 2);
		REQUIRE(vec.getData()[0] == 0);
		REQUIRE(vec.getData()[1] == 1);
	}

}

TEST_CASE("Insert element inside vector") {
	vector vec;
	
	SECTION("Insert beginning") {
		vec.insertElement(0, 0);
		vec.insertElement(1, 0);

		REQUIRE(vec.getSize() == 2);
		REQUIRE(vec.getData()[0] == 1);
		REQUIRE(vec.getData()[1] == 0);
	}

	SECTION("Insert beginning and middle") {
		vec.insertElement(0, 0);
		vec.insertElement(1, 0);
		vec.insertElement(2, 1);

		REQUIRE(vec.getSize() == 3);
		REQUIRE(vec.getData()[0] == 1);
		REQUIRE(vec.getData()[1] == 2);
		REQUIRE(vec.getData()[2] == 0);
	}
}
TEST_CASE("Remove element from vector") {
	vector vec;

	vec.insertLastElement(0);
	vec.insertLastElement(1);
	vec.insertLastElement(2);

	SECTION("Remove first element") {
		vec.removeElement(0);

		REQUIRE(vec.getSize() == 2);
		REQUIRE(vec.getData()[0] == 1);
		REQUIRE(vec.getData()[1] == 2);
	}

	SECTION("Remove middle element") {
		vec.removeElement(1);

		REQUIRE(vec.getSize() == 2);
		REQUIRE(vec.getData()[0] == 0);
		REQUIRE(vec.getData()[1] == 2);
	}

	SECTION("Remove last element") {
		vec.removeElement(2);

		REQUIRE(vec.getSize() == 2);
		REQUIRE(vec.getData()[0] == 0);
		REQUIRE(vec.getData()[1] == 1);
	}
}

/*
	Helper vector::Reducer function,
	for testing left fold
*/
static vector::Type testLeftReducer(size_t indexCounter, vector::Type accumulator, vector::Type next) {
	return accumulator + indexCounter * next;
}

TEST_CASE("Fold left with increasing values") {
	//Lambda function look online for more information

	vector vec;

	SECTION("Empty vector") {
		REQUIRE(vec.foldl(0, testLeftReducer) == 0);
	}

	SECTION("Three element vector") {
		vec.insertLastElement(1);
		vec.insertLastElement(2);
		vec.insertLastElement(3);
		REQUIRE(vec.foldl(0, testLeftReducer) == 1*0 + 1*2 + 2*3);
	}
}

/*
	Helper vector::Reducer function,
	for testing right fold
*/
static vector::Type testRightReducer(size_t indexCounter, vector::Type next, vector::Type accumulator) {
	return accumulator + indexCounter * next;
}

TEST_CASE("Fold right with increasing values") {
	//Lambda function look online for more information

	vector vec;

	SECTION("Empty vector") {
		REQUIRE(vec.foldr(0, testLeftReducer) == 0);
	}

	SECTION("Three element vector") {
		vec.insertLastElement(1);
		vec.insertLastElement(2);
		vec.insertLastElement(3);

		REQUIRE(vec.foldr(0, testRightReducer) == 3*0+2*1+1*2+0*3);
	}
}