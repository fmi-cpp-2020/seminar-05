#include "vector.hpp"
#include <assert.h>


vector::vector(size_t initalCapacity) {
	assert(initalCapacity > 0);

	this->data = new Type[initalCapacity];
	this->capacity = initalCapacity;
	this->size = 0;
}

vector::~vector() {
	delete[] this->data;
}

void vector::resize() {
	Type* newArray = new Type[this->capacity * 2];

	for (size_t i = 0; i < this->size; i++) {
		newArray[i] = this->data[i];
	}

	delete[] this->data;
	this->data = newArray;
	this->capacity *= 2;
}

void vector::shiftLeftElements(size_t index) {
	for (size_t i = index; i < this->size - 1; i++) {
		this->data[i] = this->data[i + 1];
	}
}

void vector::removeElement(size_t index) {
	assert(index < this->size);

	this->shiftLeftElements(index);

	this->size--;
}

void vector::shiftRightElements(size_t index) {
	if (this->size == 0) {
		return;
	}

	for (size_t i = this->size; i > index; i--) {
		this->data[i] = this->data[i-1];
	}
}

void vector::insertElement(Type element, size_t index) {
	assert(index <= this->size);

	if (this->size == this->capacity) {
		resize();
	}

	shiftRightElements(index);
	this->data[index] = element;
	this->size++;
}

void vector::insertLastElement(Type element) {
	insertElement(element, this->size);
}

const vector::Type* vector::getData() const {
	return this->data;
}

size_t vector::getSize() const {
	return this->size;
}

size_t vector::getCapacity() const {
	return this->capacity;
}


vector::Type vector::foldl(Type zero,
	                     Reducer reducer)
	const {
	size_t counter = 0;
	for (size_t i = 0; i < getSize(); i++) {
		zero = reducer(counter++,zero, getData()[i]);
	}

	return zero;
}
vector::Type vector::foldr(Type zero,
	                     Reducer reducer)
const {
	if (getSize() == 0) {
		return zero;
	}

	size_t counter = 0;
	for (size_t i = getSize(); i > 0; i--) {
		zero = reducer(counter++,getData()[i-1], zero);
	}

	return zero;
}
