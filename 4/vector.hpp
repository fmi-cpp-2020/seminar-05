#pragma once


/*
	A dynamiclly allocated array holding VectorType elements.
	Array is with size capacity. When it's filled with elements
	(capacity = size) the capacity is doubled.
*/
class vector {
public:
	typedef int Type;
	typedef Type (*Reducer)(size_t indexCounter, Type u, Type v);
private:

	Type* data;
	size_t capacity;
	size_t size;
	/*
		Doubles the capacity of data storage
	*/
	void resize();
	/*
		Shifts elements to the left: from size-1 to the index exlusive.
		The element index+1 -> index and so on until size-1 -> size-2.
		The index element is lost.
		
		@param index the index until which the elements to be shifted
	*/
	void shiftLeftElements(size_t index);
	/*
		Shifts elements to the right: from 0 to index exlusive.
		The element index -> index+1 and so on until 0 -> 1.
		The index element could be used as new.

		@param index the index until which the elements to be shifted
	*/
	void shiftRightElements(size_t index);
public:
	/*
		@param initialCapacity - positive value denoting the initial capacity

		@exception if initialCapacity is not positive
	*/
	vector(size_t initalCapacity = 1);
	/*
		Inserts element to the end
	*/
	void insertLastElement(Type element);
	/*
		All elements after and index are shifted right.
		The element is inserted at the index position.

		@param element to be inserted

		@param index less or equal to the vector's size
		denoting where to put the element

		@exception if index > size
	*/
	void insertElement(Type element, size_t index);
	/*
		Removes the index element and all element right of
		the index are shifted left

		@param index less than the vector's size

		@exception if index is equal or more than vector's size
	*/
	void removeElement(size_t index);

	const Type* getData() const;

	size_t getCapacity() const;

	size_t getSize() const;

	/*
		Folds left values with reducer
		example: x1,x2,..,xn -> reducer(...reducer(1,reducer(0,zero,x1),x2),...,xn)

		@param zero the first value
		@param reducer the reducer used
	*/
	Type foldl(Type zero, 
		             Reducer reducer) const;

	/*
	Folds right values with reducer
	example: x1,x2,...,xn -> reducer(...,reducer(1,x(n-1),reducer(0,xn,zero)),...)
	*/
	Type foldr(Type zero, 
		             Reducer reducer) const;

	~vector();
};


