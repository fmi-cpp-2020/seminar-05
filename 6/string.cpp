#include "string.hpp"
#include<utility>
#include <assert.h>


int string::compare(const string& other) const
{
	const char* first = this->raw();
	const char* second = other.raw();

	while ((*first) && (*second)) {
		char left = *first; first++;
		char right = *second; second++;

		if (left < right) {
			return -1;
		}
		else if (left > right) {
			return 1;
		}//continue
	}

	if (*first == 0 && *second == 0) {
		return 0;
	}
	else if (*first == 0) {
		return -1;
	}
	else {
		return 1; //*second is equal to 0
	}
	return 0;
}

string::string(const char* sequence)
{
	(*this) = sequence;
}

string::string(const string& other)
{
	assert(other.raw());

	(*this) = other;
}

string::string(string&& other) noexcept {
	(*this) = std::move(other);
}

string::~string()
{
	delete[] data;
}

size_t string::length() const
{
	return len(this->raw());
}

size_t string::len(const char* other) {

	size_t length = 0;

	while (*other) {
		length++;
		other++;
	}

	return length;
}

char string::get(size_t index) const
{
	return this->data[index];
}

char& string::get(size_t index)
{
	return this->data[index];
}

string string::concat(const string& other) const
{

	size_t leftSize = this->length();
	size_t rightSize = other.length();

	char* output = new char[leftSize + rightSize + 1];

	size_t offset = 0;
	for (size_t i = 0; i < leftSize; i++) {
		output[offset++] = (*this)[i];
	}

	for (size_t i = 0; i < rightSize; i++) {
		output[offset++] = (*this)[i];
	}

	output[offset] = '\0';

	return output;
}

const char* string::raw() const
{
	return this->data;
}

string& string::operator=(const string& other)
{
	return (*this) = other.raw();
}

string& string::operator=(const char* other) {
	assert(other);

	delete[] this->data;

	size_t length = len(other);

	char* output = new char[length + 1];

	for (size_t i = 0; i <= length; i++) {
		output[i] = other[i];
	}

	this->data = output;

	return *this;
}

string& string::operator=(string&& other) noexcept {
	delete[] this->data;
	this->data = other.data;
	other.data = nullptr;

	return *this;
}

char& string::operator[](size_t index)
{
	return get(index);
}

char string::operator[](size_t index) const
{
	return get(index);
}

string string::operator+(const string& other) const
{
	return concat(other);
}
string& string::operator+=(const string& other) {
	return (*this) = std::move((*this) + other);
}

bool string::operator==(const string& other) const
{
	return compare(other) == 0;
}

bool string::operator!=(const string& other) const
{
	return !((*this) == other);
}
bool string::operator>(const string& other) const
{
	return compare(other) > 0;
}

bool string::operator>=(const string& other) const
{
	return compare(other) >= 0;
}

bool string::operator<(const string& other) const
{
	return compare(other) < 0;
}
bool string::operator<=(const string& other) const
{
	return compare(other) <= 0;
}


std::ostream& operator<<(std::ostream& out, const string& other) {
	out << other.raw();

	return out;
}