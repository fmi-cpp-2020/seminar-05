# seminar-05
### [Seminar's Video](https://drive.google.com/file/d/1ESlehenV2oiWuDVVNqnUTZBBUKwqBc2d/view?usp=sharing)
Seminar for Informatics Group 5.

Seminar's meet: [Google Meet](https://meet.google.com/nmt-bpmk-yxq)

Assistant: Andrey Andreev (Andy)

GitLab: @aandreevh1

E-mail: aandreevh@g.fmi.uni-sofia.bg

[Facebook](https://www.facebook.com/andrei.andreev.1840/) (faster communication)
