#include "catch2.hpp"
#include "string.hpp"

static const char* testString = "abcd";
size_t testStringLength = 4;


void assertTestString(const char* str) {
	REQUIRE(str[0] == 'a');
	REQUIRE(str[1] == 'b');
	REQUIRE(str[2] == 'c');
	REQUIRE(str[3] == 'd');
}
TEST_CASE("String constructor from character sequence") {
	string str(testString);
	
	assertTestString(str.raw());
	REQUIRE(str.raw() != testString);//Assert string is actually copied
}

TEST_CASE("String copy constructor") {
	string str(testString);
	string str2 = str;

	assertTestString(str2.raw());

	REQUIRE(str2 == str);
	REQUIRE(str.raw() != testString);
}

TEST_CASE("String move constructor") {
	string str(testString);

	const char* rawPtr = str.raw();

	string str2 = std::move(str);

	REQUIRE(rawPtr == str2.raw());
	REQUIRE(str.raw() == nullptr);
}

TEST_CASE("String length") {
	string str(testString);

	REQUIRE(str.length() == testStringLength);
}

TEST_CASE("String concat") {
	string str1(testString);
	string str2(testString);
	
	string concatStr = str1.concat(str2);

	assertTestString(concatStr.raw());
	assertTestString(concatStr.raw() + testStringLength);

	REQUIRE(concatStr.length() == 2 * testStringLength);
}

TEST_CASE("String ==") {
	SECTION("Equal empty strings") {
		REQUIRE(string("") == string(""));
	}

	SECTION("Equal nonempty strings") {
		REQUIRE(string("abcd") == string("abcd"));
	}

	
	SECTION("Non equal strings") {
		REQUIRE(string("abcd") != string("abc"));
	}
}


TEST_CASE("String >") {

	SECTION("Equal empty strings") {
		REQUIRE_FALSE(string("") > string(""));
	}

	SECTION("Equal nonempty strings") {
		REQUIRE_FALSE(string("abcd") > string("abcd"));
	}

	SECTION("With bigger string") {
		REQUIRE(string("b") > string("abc"));
	}

	SECTION("With bigger string from size") {
		REQUIRE(string("abca") > string("abc"));
	}

	SECTION("With smaller string") {
		REQUIRE_FALSE(string("aa") > string("aba"));
	}
}


TEST_CASE("String <") {

	SECTION("Equal empty strings") {
		REQUIRE_FALSE(string("") < string(""));
	}

	SECTION("Equal nonempty strings") {
		REQUIRE_FALSE(string("abcd") < string("abcd"));
	}

	SECTION("With bigger string") {
		REQUIRE_FALSE(string("b") < string("abc"));
	}

	SECTION("With bigger string from size") {
		REQUIRE_FALSE(string("abca") < string("abc"));
	}

	SECTION("With smaller string") {
		REQUIRE(string("aa") < string("aba"));
	}
}