#pragma once
#include<iostream>

/*
	Represents a safe pointer box
	for character sequences
*/
class string {
	/*
	* Finds the length of a character sequence
	* 
	* @param other the character sequence
	*/
	static size_t len(const char* other);

	char* data = nullptr;
	/*
		Compares this string with other lexicographically

		@param other the other sequence
	*/
	int compare(const string&) const;
public:

	/*
		Creates a string from a character seqhence

		@param other the character sequence

		@exception if the character sequence is nullptr
	*/
	string(const char* other);

	string(const string& other);
	string(string&& other) noexcept;
	~string();

	/*
		@return the length of the string
	*/
	size_t length() const;
	/*
		The index character of the string

		@param index the index

		@return the character

		@note due to performance the index is
			  not checked if in bounds

	*/
	char get(size_t index) const;
	/*
		The index character of the string

		@param index the index

		@return the character

		@note due to performance the index is
			  not checked if in bounds
	*/
	char& get(size_t);
	/*
	 Creates a new string from concatenation
	 of this with other

	 @param other the other string

	 @return the resulted concatenated string
	*/
	string concat(const string& other) const;
	/*
		@return the raw character sequence represented
		by the string
	*/
	const char* raw() const;

	string& operator=(const string&);
	string& operator=(string&&) noexcept;
	string& operator=(const char*);

	/*
	 Concatenates this string by extending it
	 with the other

	 @param other the other string

	 @return this
	*/
	string& operator+=(const string&);

	/*
		The index character of the string

		@param index the index

		@return the character

		@note due to performance the index is
			  not checked if in bounds
	*/
	char& operator[](size_t);
	/*
		The index character of the string

		@param index the index

		@return the character

		@note due to performance the index is
			  not checked if in bounds
	*/
	char operator[](size_t) const;

	/*
	 Creates a new string from concatenation
	 of this with other

	 @param other the other string

	 @return the resulted concatenated string
	*/
	string operator+(const string& other) const;

	/*
		@return if this string is equal to the other lexicographically
	*/
	bool operator==(const string& other) const;
	/*
		@return if this string is not equal to the other lexicographically
	*/
	bool operator!=(const string& other) const;
	/*
		@return if this string is bigger to the other lexicographically
	*/
	bool operator> (const string& other) const;
	/*
		@return if this string is bigger or equal to the other lexicographically
	*/
	bool operator>= (const string& other) const;
	/*
		@return if this string is less to the other lexicographically
	*/
	bool operator< (const string& other) const;
	/*
		@return if this string is less or equal to the other lexicographically
	*/
	bool operator<= (const string& other) const;

};

std::ostream& operator<<(std::ostream&, const string&);