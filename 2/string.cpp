#include "string.hpp"
#include <new>

unsigned strLen(const char* text){
	if (text == nullptr) {
		return 0;
	}

	unsigned length = 0;

	while (*text) {
		length++;
		text++;
	}

	return length;
}

bool strEquals(const char* left, const char* right) {
	if (left == right) {
		return true;
	}

	if (left == nullptr || right == nullptr) {
		return false;
	}

	/*
	TODO: solution without using 'strLen'
	*/
	unsigned leftLength = strLen(left);
	unsigned rightLength = strLen(right);

	if (leftLength != rightLength) {
		return false;
	}

	for (unsigned i = 0; i < leftLength; i++) {
		if (left[i] != right[i]) {
			return false;
		}
	}

	return true;
}

char* strCopy(const char* text) {
	if (!text) {
		return nullptr;
	}

	unsigned length = strLen(text);

	char* output = new (std::nothrow)char[length + 1];

	if (!output) {
		return nullptr;
	}

	for (unsigned i = 0; i <= length; i++) {
		output[i] = text[i];
	}

	return output;
}
