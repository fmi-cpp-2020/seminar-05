/*
TODO: setup catch2.hpp
*/

#define CATCH_CONFIG_MAIN 
#include "catch2.hpp"
#include "string.hpp"

TEST_CASE("String length", "[strLen]") {
	REQUIRE(strLen(nullptr) == 0);
	REQUIRE(strLen("") == 0);
	REQUIRE(strLen("abc") == 3);
}

TEST_CASE("String equals", "[strEquals]") {
	REQUIRE(strEquals(nullptr, "") == false);
	REQUIRE(strEquals("", nullptr) == false);
	REQUIRE(strEquals("a", "ab") == false);
	REQUIRE(strEquals("aa", "ab") == false);

	REQUIRE(strEquals(nullptr, nullptr) == true);
	REQUIRE(strEquals("abc", "abc") == true);
}

TEST_CASE("String copy", "[strCopy]") {
	//Before each section
	char* copiedString = nullptr;
	
	SECTION("Copy nullptr string") {
		const char* string = nullptr;

		copiedString = strCopy(string);

		REQUIRE(strEquals(string, copiedString) == true);

	}

	SECTION("Copy empty string") {
		const char* string = "";

		copiedString = strCopy(string);

		REQUIRE(strEquals(string, copiedString) == true);
	}

	SECTION("Copy regular string") {
		const char* string = "abc";

	    copiedString = strCopy(string);

		REQUIRE(strEquals(string, copiedString) == true);
	}

	//After each section
	delete[] copiedString;
}

/*
TODO: add tests for: strCmp, strConcat, strSort
*/
