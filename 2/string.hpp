#ifndef STRING_HEADER
#define STRING_HEADER


/*
	Finds the length of a character sequence
*/
unsigned strLen(const char* text);
/*
	If two character sequences are equal
*/
bool strEquals(const char* left, const char* right);
/*
	Copies a character sequence
*/
char* strCopy(const char* text);

/*
TODO: strCmp - compare two strings lexicographically
*/

/*
TODO: strConcat - concatenate two strings
*/

/*
TODO: strSort - sort strings lexicographically in increasing order: "a","b",...
*/
#endif
