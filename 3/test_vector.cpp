#include "catch2.hpp"
#include "vector.hpp"


TEST_CASE("Vector initialization","[vectorInit]") {
	vector vec;

	vectorInit(vec, 1);

	REQUIRE(vec.capacity == 1);
	REQUIRE(vec.size == 0);

	vectorDelete(vec);
}

TEST_CASE("Insert last element into vector", "[vectorInsertLastElement]") {
	vector vec;
	vectorInit(vec, 1);

	SECTION("Without resize") {
		vectorInsertLastElement(vec, 0);

		REQUIRE(vec.capacity == 1);
		REQUIRE(vec.size == 1);
		REQUIRE(vec.data[0] == 0);

	
	} 

	SECTION("With resize") {
		vectorInsertLastElement(vec, 0);
		vectorInsertLastElement(vec, 1);

		REQUIRE(vec.capacity == 2);
		REQUIRE(vec.size == 2);
		REQUIRE(vec.data[0] == 0);
		REQUIRE(vec.data[1] == 1);
	}

	vectorDelete(vec);
}

//TODO: test: vectorInsertElement 

TEST_CASE("Remove element from vector") {

	vector vec;
	vectorInit(vec, 1);

	vectorInsertLastElement(vec, 0);
	vectorInsertLastElement(vec, 1);
	vectorInsertLastElement(vec, 2);

	SECTION("Remove first element") {
		vectorRemoveElement(vec, 0);

		REQUIRE(vec.size == 2);
		REQUIRE(vec.data[0] == 1);
		REQUIRE(vec.data[1] == 2);
	}

	SECTION("Remove middle element") {
		vectorRemoveElement(vec, 1);

		REQUIRE(vec.size == 2);
		REQUIRE(vec.data[0] == 0);
		REQUIRE(vec.data[1] == 2);
	}

	SECTION("Remove last element") {
		vectorRemoveElement(vec, 2);

		REQUIRE(vec.size == 2);
		REQUIRE(vec.data[0] == 0);
		REQUIRE(vec.data[1] == 1);
	}

	vectorDelete(vec);
}