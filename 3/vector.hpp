#pragma once

typedef int VectorType;

/*
TODO: add description
*/
struct vector {
	VectorType* data;
	size_t capacity;
	size_t size;
};

/*
	Initializes vector with an initial capacity
	@param vector - vector to be initialized
	@param initialCapacity - positive value denoting the initial capacity
*/
void vectorInit(vector& vector,size_t initalCapacity = 1);

/*
	TODO: write description
*/
void vectorInsertLastElement(vector& vector, VectorType element);

/*
	TODO: write description
*/
void vectorInsertElement(vector& vector, VectorType element, size_t index);

/*
	TODO: write description
*/
void vectorRemoveElement(vector& vector, size_t index);

/*
	Deletes internally allocated memory of a vector
*/
void vectorDelete(vector& vector);
