#include "vector.hpp"
#include <assert.h>

void vectorInit(vector& vector, size_t initalCapacity) {
	assert(initalCapacity > 0);

	vector.data = new VectorType[initalCapacity];
	vector.capacity = initalCapacity;
	vector.size = 0;
}

void vectorDelete(vector& vector) {
	delete[] vector.data;
	vector.data = nullptr;
}

static void vectorResize(vector& vector) {
	VectorType* newArray = new VectorType[vector.capacity * 2];
	for (size_t i = 0; i < vector.size; i++) {
		newArray[i] = vector.data[i];
	}

	delete[] vector.data;
	vector.data = newArray;
	vector.capacity *= 2;
}


static void vectorShiftLeftElements(vector& vector, size_t index) {
	for (size_t i = index; i < vector.size - 1; i++) {
		vector.data[i] = vector.data[i + 1];
	}
}

void vectorRemoveElement(vector& vector, size_t index) {
	assert(index < vector.size);

	vectorShiftLeftElements(vector, index);
	
	vector.size--;
}


static void vectorShiftRightElements(vector& vector, size_t index) {
	if (vector.size == 0) {
		return;
	}

	for (size_t i = vector.size-1; i >= index ; i--) {
		vector.data[i + 1] = vector.data[i];
	}
}
void vectorInsertElement(vector& vector, VectorType element, size_t index) {
	assert(index <= vector.size);

	if (vector.size == vector.capacity) {
		vectorResize(vector);
	}

	vectorShiftRightElements(vector, index);
	vector.data[index] = element;
	vector.size++;
}

void vectorInsertLastElement(vector& vector, VectorType element) {
	vectorInsertElement(vector, element, vector.size);
}
