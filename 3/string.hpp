#pragma once
/**
	Finds the length of a character sequence 

	@param string the character sequence

	@return the length of the sequence

	@exception if string is a nullptr
*/
size_t stringLength(const char* string);

/**
	Finds if two character sequences are equal
	
	@param first the first character sequence
	@param second the second character sequence

	@return if both sequences are lexicographically equal 
	
	@exception if any parameter is a nullptr
*/
bool stringEquals(const char* first, const char* second);

/**
	Copies a character sequence into a new one 
	or null if the given sequence is null

	@param string  the character sequence to be copied

	@return the new character sequence or null

	@exception if any parameter is a nullptr
*/
char* stringCopy(const char* string);

/** 
Compares two character sequences lexicographically

	@param first the first character sequence
	@param second the second character sequence

	@return the comperator value: 
			-1 if first < second,
			 0 if first = second,
			 1 if first > second

	@exception if any parameter is a nullptr
*/
int stringCompare(const char* first, const char* second);

/**
Concatenates two character sequences into a new one:
	"a" + "b" = "ab"

	@param left the character sequence to be concatnated on the left
	@param right the character sequecne to be concatenated on the right

	@return the new concatenated character sequence

	@exception if any parameter is a nullptr
*/
char* stringConcat(const char* left, const char* right);


/**
Sorts a character sequence array array lexicographically in an ascending order
	
	@param strings the character sequence array
	@param n the length of the strings array

	@return the strings array sorted

	@exception if the character sequence array or any of the sequences is nullptr
*/
const char** sortLexicographically(const char** strings, size_t n);