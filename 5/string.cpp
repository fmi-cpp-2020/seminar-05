#include "string.hpp"
#include <assert.h>

size_t stringLength(const char* string) {
	assert(string);

	size_t length = 0;

	while (*string) {
		length++;
		string++;
	}

	return length;
}

bool stringEquals(const char* first, const char* second) {
	return stringCompare(first, second) == 0;
}

char* stringCopy(const char* string) {
	assert(string);

	size_t length = stringLength(string);

	char* output = new char[length + 1];

	for (size_t i = 0; i <= length; i++) {
		output[i] = string[i];
	}

	return output;
}


int stringCompare(const char* first, const char* second) {
	assert(first, second);

	while ((*first) && (*second)) {
		char left = *first; first++;
		char right = *second; second ++;

		if (left < right) {
			return -1;
		}
		else if (left > right) {
			return 1;
		}//continue
	}

	if (*first == 0 && *second == 0) {
		return 0;
	}
	else if (*first == 0) {
		return -1;
	}
	else {
		return 1; //*second is equal to 0
	}
}


char* stringConcat(const char* left, const char* right) {

	size_t leftSize = stringLength(left);
	size_t rightSize = stringLength(right);

	char* output = new char[leftSize + rightSize + 1];

	size_t offset = 0;
	for (size_t i = 0; i < leftSize; i++) {
		output[offset++] = left[i];
	}

	for (size_t i = 0; i < rightSize; i++) {
		output[offset++] = right[i];
	}

	output[offset] = '\0';

	return output;
}

static void assertStringsNotNullptr(const char** strings, size_t n) {
	assert(strings);
	for (size_t i = 0; i < n; i++) {
		assert(strings[i] );
	}
}

static void swap(const char*& u, const char*& v) {
	const char* temp = u;

	u = v;
	v = temp;
}

const char** sortLexicographically(const char** strings, size_t n) {
	assertStringsNotNullptr(strings, n);

	//O(n^2) - not most efficient, but easy to understand
	for (size_t i = 0; i < n; i++) {
		for (size_t j = i + 1; j < n; j++) {
			//stable sort
			if (stringCompare(strings[i], strings[j]) > 0) {
				swap(strings[i], strings[j]);
			}
		}
	}

	return strings;
}