#include "catch2.hpp"
#include "string.hpp"

TEST_CASE("String length", "[stringLen]") {
	REQUIRE(stringLength("") == 0);
	REQUIRE(stringLength("abc") == 3);
}

TEST_CASE("String equals", "[stringEquals]") {
	REQUIRE(stringEquals("a", "ab") == false);
	REQUIRE(stringEquals("aa", "ab") == false);

	REQUIRE(stringEquals("abc", "abc") == true);
}

TEST_CASE("String copy", "[stringCopy]") {
	//Before each section
	char* copiedString = nullptr;


	SECTION("Copy empty string") {
		const char* string = "";

		copiedString = stringCopy(string);

		REQUIRE(stringEquals(string, copiedString) == true);
	}

	SECTION("Copy regular string") {
		const char* string = "abc";

		copiedString = stringCopy(string);

		REQUIRE(stringEquals(string, copiedString) == true);
	}

	//After each section
	delete[] copiedString;
}

TEST_CASE("Comparing string lexicographically", "[stringCompare]") {

	    REQUIRE(stringCompare("", "a") == -1);
	    REQUIRE(stringCompare("abc", "abd") == -1);

		REQUIRE(stringCompare("", "") == 0);
		REQUIRE(stringCompare("caf", "caf") == 0);

		REQUIRE(stringCompare("a", "") == 1);
		REQUIRE(stringCompare("ab", "a") == 1);
		REQUIRE(stringCompare("ac", "ab") == 1);
}

TEST_CASE("Concatenate stirngs", "[stringConcat]") {
	char* concatenatedString = nullptr;

	SECTION("Empty strings") {
		concatenatedString = stringConcat("", "");
		REQUIRE(stringEquals(concatenatedString, "") == true);
	}

	SECTION("First string empty") {
		concatenatedString = stringConcat("", "a");
		REQUIRE(stringEquals(concatenatedString, "a") == true);
	}

	SECTION("Second string empty") {
		concatenatedString = stringConcat("a", "");
		REQUIRE(stringEquals(concatenatedString, "a") == true);
	}

	SECTION("Equally sized strings") {
		concatenatedString = stringConcat("a", "b");
		REQUIRE(stringEquals(concatenatedString, "ab") == true);
	}

	delete[] concatenatedString;
}

TEST_CASE("Sort stirngs lexicographically", "[sortLexicographically]") {
	const char** stringArray = nullptr;

	SECTION("Single element array") {
		stringArray = new const char* [1];
		stringArray[0] = "a";

		sortLexicographically(stringArray, 1);
		REQUIRE(stringEquals(stringArray[0], "a") == true);
	}


	SECTION("Multiple element array") {
		stringArray = new const char* [3];
		stringArray[0] = "c";
		stringArray[1] = "a";
		stringArray[2] = "b";

		sortLexicographically(stringArray, 3);

		REQUIRE(stringEquals(stringArray[0], "a") == true);
		REQUIRE(stringEquals(stringArray[1], "b") == true);
		REQUIRE(stringEquals(stringArray[2], "c") == true);
	}

	delete[] stringArray;
}